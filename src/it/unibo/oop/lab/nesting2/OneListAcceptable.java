package it.unibo.oop.lab.nesting2;

import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {

	private final List<T> listaAcc;

	public OneListAcceptable(final List<T> lista) {
		this.listaAcc = lista;
	}

	@Override
	public Acceptor<T> acceptor() {
		return new Acceptor<T>() {
			private int count;

			public void accept(T newElement) throws ElementNotAcceptedException {
				count++;
				for (T elem : listaAcc) {
					if (newElement.equals(elem)) {
						return;
					}
				}
				throw new ElementNotAcceptedException(newElement);
			}

			public void end() throws EndNotAcceptedException {
				if (count != listaAcc.size()) {
					throw new EndNotAcceptedException();
				}
			}
		};
	}
}
